import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib
from networkx.algorithms.cluster import clustering
import numpy as np
import random as rand
import networkx as nx
import networkx.drawing as nxd
import math
import os

from numpy.core.defchararray import array
import graph_building as gb
from multiprocessing import Process, Queue
from mpl_toolkits.mplot3d import Axes3D
from networkx.algorithms import community
import PySimpleGUI as sg
from scipy.stats import poisson as poi
from scipy.stats import powerlaw


def import_Facebook(G, nodes, max_op, seed):
    """
    Import the facebook network from government_edges.csv.
    G: Graph in which the network will be saved
    nodes: array of all nodes
    max_op: limit for the opinion
    seed: random seed
    """
    rand.seed(seed)
    f = open("government_edges.csv", "r")
    line = f.readline()
    line = f.readline()
    pop = 0
    while line.endswith("\n"):
        line = line[:-1]
        actors = line.rsplit(",")
        actors[0] = int(actors[0])
        actors[1] = int(actors[1])
        for n in range(2):
            if nodes[actors[n]]["opinion"] == max_op + 1:
                o = rand.gauss(0, 1)
                if o > max_op:
                    o = max_op
                elif o < -max_op:
                    o = -max_op
                nodes[actors[n]] = (o, 0, 0, 0, actors[n], 0, 0, 0)
                G.add_node(
                    actors[n],
                    opinion=o,
                    social=0,
                    y=0,
                    x=0,
                    modul=0,
                    cluster=0,
                    clustering=0,
                )
            if actors[0] != actors[1]:
                G.add_edge(actors[0], actors[1])
        line = f.readline()

    f.close()

    pop = len(G.nodes)
    in_d = 0
    out_d = 0
    for n in G.nodes:
        out_d += G.out_degree(n)
        in_d += G.in_degree(n)
    in_d /= pop
    out_d /= pop
    print("all:")
    print("pop: " + str(pop))
    print("out degree: " + str(out_d))
    print("in degree: " + str(in_d))

    return (
        nodes,
        G,
    )


def build_G(G, nodes, max_op, dis, seed, alpha=2, beta=4, social_scale=0.5):
    """
    Create nodes for a network.
    G: Graph in which the network will be saved
    nodes: array of all nodes
    max_op: limit for the opinion
    seed: random seed
    alpha: alpha for beta-distribution
    beta: beta for beta-distribution
    social_scale: social_scale of beta-distribution
    """
    rand.seed(seed)
    np.random.seed(seed)
    pop = len(nodes)
    fac = np.sqrt(pop)
    square = 0
    sum = 0

    # main loop to create all nodes with random attributes
    for x in range(pop):
        o = rand.gauss(0, 1)
        if o > max_op:
            o = max_op
        elif o < -max_op:
            o = -max_op
        if dis == "Gauss distribution":
            s = abs(rand.gauss(10, 3))
        elif dis == "Beta distribution":
            s = rand.betavariate(alpha, beta)
            square += s ** 2
            sum += 2 * s
        elif dis == "Powerlaw":
            s = powerlaw.rvs(0.3)
        else:
            s = poi.rvs(2)

        y = rand.random() * fac
        w = rand.random() * fac
        nodes[x] = (o, s, y, w, 0, 0, 0, 0)

    # normalization of distribution
    nodes = np.sort(nodes, order="y")

    aim = social_scale * pop
    x = 0
    try:
        x = (-sum + math.sqrt(sum ** 2 - 4 * 1000 * (square - aim))) / 2000
    except:
        x = -sum / 2000
        aim = (square - (sum ** 2) / 4000) / pop
        print("failed normalization! Increase social range to:", aim)

    counter = 0
    for n in nodes:
        n["social"] += x
        counter += n["social"] ** 2
    for n in range(pop):
        nodes[n][4] = n

    for x in range(pop):
        G.add_node(
            x,
            opinion=nodes[x][0],
            social=nodes[x][1],
            y=nodes[x][2],
            x=nodes[x][3],
            modul=0,
            cluster=0,
            clustering=0,
        )

    return (G, nodes)


def connect_G(nodes, G, rand_per, degree, cc, cc_mod):
    """
    Connect given nodes.
    G: Graph in which the network will be saved
    nodes: array of all nodes
    rand_per: randomization percentage for edges to modify the clustering coefficient
    degree: average degree of the network
    cc: clustering coefficient the network will have
    cc_mod: mode whether to use rand_per or cc
    """
    pop = len(nodes)

    neighbors = np.full(pop, list)
    show_neigh = neighbors.copy()
    for neigh in range(pop):
        neighbors[neigh] = list()
        show_neigh[neigh] = list()

    fac = np.sqrt(pop)
    edges_con = 0

    # scale social radius to match given degree
    su = 0
    for n in nodes:
        su += n["social"] ** 2
    su *= math.pi

    scale = degree * pop / su
    nodes["social"] *= np.sqrt(scale)
    print("average A: " + str(su / pop))
    print("scale: " + str(scale))
    # main loop to connect every node x with all it's neighbors
    for x in range(pop):
        s = nodes[x][1]
        h = nodes[x][2]
        w = nodes[x][3]
        temp = nodes["y"].copy()
        start = np.searchsorted(temp, h - s)
        for i in range(start, pop):
            if i != x:
                normal_diff = math.sqrt(
                    pow(h - nodes[i][2], 2) + pow(w - nodes[i][3], 2)
                )
                overb_diff = math.sqrt(
                    pow(h - nodes[i][2], 2) + pow(abs(w - nodes[i][3]) - fac, 2)
                )
                if normal_diff <= overb_diff and normal_diff <= s:
                    # outgoing edges
                    neighbors[x].append(i)
                    show_neigh[x].append(i)
                    edges_con += 1
                elif overb_diff < normal_diff and overb_diff <= s:
                    neighbors[x].append(i)
                    edges_con += 1
                if abs(h - nodes[i][2]) > s:
                    break
        # edges over the edge
        if h - s < 0 or h + s > fac:
            start = 0
            if h - s < 0:
                start = np.searchsorted(temp, fac - abs(h - s))
            for i in range(start, pop):
                normal_diff = math.sqrt(
                    pow(abs(h - nodes[i][2]) - fac, 2) + pow(w - nodes[i][3], 2)
                )
                overb_diff = math.sqrt(
                    pow(abs(h - nodes[i][2]) - fac, 2)
                    + pow(abs(w - nodes[i][3]) - fac, 2)
                )
                min_diff = min(normal_diff, overb_diff)
                if min_diff <= s:
                    neighbors[x].append(i)
                    edges_con += 1
                if nodes[i][2] > abs(h - s) and h - s > 0:
                    break
    print("Edges: " + str(edges_con))

    # rewiring 5% of edges
    if rand_per == 0:
        a_d = edges_con / pop
        w = np.empty(pop, dtype=float)
        for n in range(pop):
            d_k = len(neighbors[n])
            if d_k < a_d:
                w[n] = 1
            else:
                w[n] = d_k - a_d + 1
        counter = 0
        for x in range(int(edges_con * 0.05)):
            found = False
            while not found:
                n1, n2 = rand.choices(nodes, weights=w, k=2)
                if not n2[4] in neighbors[n1[4]]:
                    if len(neighbors[n1[4]]) > 0:
                        neighbors[n1[4]].pop(0)
                    neighbors[n1[4]].append(n2[4])
                    found = True

    # adding edges in graph
    for x in range(pop):
        for i in neighbors[x]:
            G.add_edge(x, i)

    # rewiring to match the clustering coefficient
    cc_temp = 0
    G_undirect = G.to_undirected()
    for i in range(pop):
        cc_temp += local_clustering(G, G_undirect, i)
    G_sub = nx.subgraph(G, np.arange(0, int(pop)))
    white_edges = list(G_sub.edges())
    edges_rand = 0
    dist = int((cc_temp / pop - cc) * 5000)
    if dist == 0:
        dist = 1

    if cc_mod == 1:
        print("clustering_coefficient:")
        # loop to rewire edges to match given clustering coefficient (slow)
        while cc_temp / pop > cc:
            print(cc_temp / pop, end="\r")
            try:
                edge = rand.choice(white_edges)
            except:
                print("clustering coefficient too low!")
                break
            white_edges.remove(edge)
            s = edge[0]
            t = edge[1]
            found = False
            while not found:
                # new_t = int(rand.random() * (pop - pop / 4))
                new_t = int(rand.random() * pop)
                if new_t not in list(G[s]):
                    found = True

            G.remove_edge(s, t)
            G.add_edge(s, new_t)
            if s not in list(G[t]):
                G_undirect.remove_edge(s, t)
            G_undirect.add_edge(s, new_t)
            if t in show_neigh[s]:
                show_neigh[s].remove(t)
                show_neigh[s].append(new_t)
            if edges_rand != 0 and edges_rand % dist == 0:
                cc_temp_new = 0
                for i in range(pop):
                    cc_temp_new += local_clustering(G, G_undirect, i)
                step = (cc_temp - cc_temp_new) / (dist * pop)
                cc_temp = cc_temp_new
                dist = int((cc_temp / pop - cc) / (step * 2))
                if dist == 0:
                    dist = 1
            edges_rand += 1
    elif cc_mod == 0:
        # rewiring given percentage of nodes (fast)
        edges = rand.sample(
            list(G_sub.edges()), k=int(len(list(G_sub.edges())) * rand_per)
        )
        edges_rand = len(edges)
        for edge in edges:
            s = edge[0]
            t = edge[1]
            found = False
            while not found:
                new_t = int(rand.random() * pop)
                if new_t not in list(G[s]):
                    found = True

            G.remove_edge(s, t)
            G.add_edge(s, new_t)
            if s not in list(G[t]):
                G_undirect.remove_edge(s, t)
            G_undirect.add_edge(s, new_t)
            if t in show_neigh[s]:
                show_neigh[s].remove(t)
                show_neigh[s].append(new_t)

    print("rewired edges: " + str(edges_rand))
    return (G, show_neigh)


# test method not of relevance for bachelor
def rewire(G, nodes, mod_num, seed):
    """
    rewire network.
    G: Graph in which the network will be saved
    nodes: array of all nodes
    mod_num: number of modules
    seed: random seed
    """
    rand.seed(seed)
    pop = len(nodes)
    mods_list = np.full(mod_num, list)
    for m in range(mod_num):
        mods_list[m] = list()
    for n in G.nodes:
        mods_list[nodes[n]["modul"]].append(n)

    number = 0
    counter = 0
    while number < 0.25 * pop:
        number += len(mods_list[counter])
        counter += 1
    counter -= 1
    for x in range(1, counter):
        mods_list[0] += mods_list[x]
    print("rewiring:")
    for x in range(pop * 5):
        print(x, end="\r")
        edge = rand.choice(list(G.edges()))
        s = edge[0]
        module = nodes[s]["modul"]
        found = False
        counter = 0
        while not found and counter < len(mods_list[0]):
            if module < counter:
                t = rand.choice(mods_list[0])
            else:
                t = rand.choice(mods_list[module])
            found = t not in G[s]
            counter += 1
        G.remove_edge(s, edge[1])
        G.add_edge(s, t)
    return G


def local_clustering(G, G_undirect, node):
    """
    calculate the local clustering coefficient of a node.
    G: Graph in which the network will be saved
    G_undirect: undirect version of G
    node: node whose clustering coefficient will be calculated
    """
    nbrs = list(G_undirect[node])
    if len(nbrs) > 1:
        g_temp = nx.subgraph(G, nbrs)
        edges = len(g_temp.edges)
        cc = edges / (len(nbrs) * (len(nbrs) - 1))
        if cc > 1:
            cc = 1
    else:
        cc = 1
    return cc


def spaces(x):
    """
    Create x spaces.
    x: number of spaces
    """
    res = ""
    for n in range(x):
        res += " "
    return res


def polarization_op(nodes, face=False, whitelist=None):
    """
    Calculate polarization of given network.
    nodes: array of all nodes
    face: If network is the facebook network
    whitelist: list of nodes for the calculation in the facebook network
    """
    groups = dict()
    num = len(nodes)
    if face:
        num = len(whitelist)
    # loop to group nodes into different opinions
    for n in nodes:
        if not face or n["id"] in whitelist:
            op = round(n["opinion"], 1)
            g = groups.get(op, -1)
            if g == -1:
                groups.update({op: 1 / num})
            else:
                groups[op] = g + 1 / num

    # calculating polarization with groups
    pol = 0
    groups2 = groups.copy()
    for op, x in groups.items():
        groups2.pop(op)
        for op2, x2 in groups2.items():
            diff = abs(op - op2)
            pol += diff * (pow(x, 2) * x2 + pow(x2, 2) * x)
    return pol


def set_size(fraction=1, subplots=(1, 1)):
    """
    Set size of a figure.
    fraction: Fraction of the figure in Latex
    subplots: subplots of the figure
    """
    # calculating width and height of graph
    fig_width_pt = 423.95 * fraction
    inches_per_pt = 1 / 72.27

    golden_ratio = (5 ** 0.5 - 1) / 2

    fig_width_in = fig_width_pt * inches_per_pt
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)


def make_gui():
    """
    Make GUI for social_networks.py.
    """
    layout = [
        [sg.Checkbox("save", size=(30, 3), enable_events=True, key="save")],
        [
            sg.Checkbox(
                "Import Facebook pages",
                size=(30, 3),
                enable_events=True,
                key="facebook",
            )
        ],
        [
            sg.pin(
                sg.Radio(
                    "Compare Clustering Coefficient",
                    "compare",
                    size=(30, 3),
                    enable_events=True,
                    key="compare_cc",
                )
            ),
        ],
        [
            sg.pin(sg.Text("1. Randomization", key="max_cc_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0", size=(10, 1), enable_events=True, key="max_cc", visible=False
                )
            ),
        ],
        [
            sg.pin(sg.Text("2. Randomization", key="min_cc_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "1", size=(10, 1), enable_events=True, key="min_cc", visible=False
                )
            ),
        ],
        [
            sg.pin(
                sg.Radio(
                    "Compare Density",
                    "compare",
                    size=(30, 3),
                    enable_events=True,
                    key="compare_d",
                )
            ),
        ],
        [
            sg.pin(sg.Text("1. Degree", key="max_degree_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "20",
                    size=(10, 1),
                    enable_events=True,
                    key="max_degree",
                    visible=False,
                )
            ),
        ],
        [
            sg.pin(sg.Text("2. Degree", key="min_degree_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "5",
                    size=(10, 1),
                    enable_events=True,
                    key="min_degree",
                    visible=False,
                )
            ),
        ],
        [
            sg.Radio(
                "Compare Distribution",
                "compare",
                size=(30, 3),
                enable_events=True,
                key="compare_dis",
            ),
        ],
        [
            sg.pin(
                sg.Combo(
                    values=[
                        "Powerlaw",
                        "Beta distribution",
                        "Gauss distribution",
                        "Poisson distribution",
                    ],
                    size=(25, 1),
                    default_value="Beta distribution",
                    key="dis_1",
                    visible=False,
                    enable_events=True,
                )
            ),
            sg.pin(sg.Text("Alpha", key="alpha1_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0.5", size=(10, 1), enable_events=True, key="alpha1", visible=False
                )
            ),
            sg.pin(sg.Text("Beta", key="beta1_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0.5", size=(10, 1), enable_events=True, key="beta1", visible=False
                )
            ),
            sg.pin(sg.Text("Social scale", key="scale1_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0.5", size=(25, 1), enable_events=True, key="scale1", visible=False
                )
            ),
        ],
        [
            sg.pin(
                sg.Combo(
                    values=[
                        "Powerlaw",
                        "Beta distribution",
                        "Gauss distribution",
                        "Poisson distribution",
                    ],
                    size=(25, 1),
                    default_value="Beta distribution",
                    key="dis_2",
                    visible=False,
                    enable_events=True,
                )
            ),
            sg.pin(sg.Text("Alpha", key="alpha2_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "2", size=(10, 1), enable_events=True, key="alpha2", visible=False
                )
            ),
            sg.pin(sg.Text("Beta", key="beta2_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "6", size=(10, 1), enable_events=True, key="beta2", visible=False
                )
            ),
            sg.pin(sg.Text("Social scale", key="scale2_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0.5", size=(25, 1), enable_events=True, key="scale2", visible=False
                )
            ),
        ],
        [
            sg.pin(
                sg.Radio(
                    "Compare Facebook",
                    "compare",
                    size=(30, 3),
                    enable_events=True,
                    key="compare_face",
                )
            ),
        ],
        [
            sg.Radio(
                "Compare Nothing",
                "compare",
                size=(30, 3),
                enable_events=True,
                key="compare_nothing",
            )
        ],
        [
            sg.pin(sg.Text("Population", key="pop_t")),
            sg.pin(sg.InputText("1000", size=(25, 1), enable_events=True, key="pop")),
        ],
        [
            sg.pin(
                sg.Radio(
                    "Random Percentage",
                    "cc_mod",
                    size=(30, 3),
                    enable_events=True,
                    key="rand_per_rad",
                )
            ),
        ],
        [
            sg.pin(
                sg.Text("Randomization Percentage", key="rand_per_t", visible=False)
            ),
            sg.pin(
                sg.InputText(
                    "0", size=(25, 1), enable_events=True, key="rand_per", visible=False
                )
            ),
        ],
        [
            sg.pin(
                sg.Radio(
                    "Clustering Coefficient",
                    "cc_mod",
                    size=(30, 3),
                    enable_events=True,
                    key="cc_rad",
                )
            ),
        ],
        [
            sg.pin(sg.Text("Clustering Coefficient", key="cc_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "1", size=(25, 1), enable_events=True, key="cc", visible=False
                )
            ),
        ],
        [
            sg.pin(sg.Text("Seed", key="seed_t")),
            sg.pin(sg.InputText("1", size=(25, 1), enable_events=True, key="seed")),
        ],
        [
            sg.pin(sg.Text("Degree", key="degree_t")),
            sg.pin(sg.InputText("20", size=(25, 1), enable_events=True, key="degree")),
        ],
        [
            sg.pin(sg.Text("Minimal opinion difference", key="min_op_diff_t")),
            sg.pin(
                sg.InputText("1", size=(25, 1), enable_events=True, key="min_op_diff")
            ),
        ],
        [
            sg.pin(
                sg.Combo(
                    values=[
                        "Powerlaw",
                        "Beta distribution",
                        "Gauss distribution",
                        "Poisson distribution",
                    ],
                    size=(25, 1),
                    default_value="Powerlaw",
                    key="dis",
                    enable_events=True,
                )
            )
        ],
        [
            sg.pin(sg.Text("Alpha", key="alpha_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "2", size=(25, 1), enable_events=True, key="alpha", visible=False
                )
            ),
        ],
        [
            sg.pin(sg.Text("Beta", key="beta_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "4", size=(25, 1), enable_events=True, key="beta", visible=False
                )
            ),
        ],
        [
            sg.pin(sg.Text("Social scale", key="scale_t", visible=False)),
            sg.pin(
                sg.InputText(
                    "0.5", size=(25, 1), enable_events=True, key="scale", visible=False
                )
            ),
        ],
        [sg.Button("OK")],
    ]
    keys = list()
    for elem in layout:
        for elem2 in elem:
            if elem2.Key == None:
                keys.append(elem2.Rows[0][0].Key)
            else:
                keys.append(elem2.Key)

    show = True
    facebook = False
    compare_cc = False
    compare_face = False
    max_cc = 1
    min_cc = 0
    compare_d = False
    max_d = 20
    min_d = 5
    pop = 1000
    rand_per = 0
    seed = 1
    degree = 20
    min_op_diff = 1
    op_lim = 3
    distribution = ""
    compare_dis = False
    dis_1 = ""
    dis_2 = ""
    alpha = 2
    beta = 4
    alpha1 = 2
    beta1 = 6
    alpha2 = 0.5
    beta2 = 0.5
    scale = 0.5
    scale1 = 0.5
    scale2 = 0.5
    cc = 1
    cc_mod = -1

    # Create the window
    window = sg.Window("Network Simulation", layout)
    # Create an event loop
    while True:
        event, values = window.read()

        if event == "facebook":
            facebook = values["facebook"]
            whitelist = [
                "facebook",
                "save",
                "OK",
                "min_op_diff",
                "min_op_diff_t",
            ]
            for elem in keys:
                if not elem in whitelist:
                    window[elem].update(visible=not facebook)

        compare_cc = values["compare_cc"]
        if not facebook:
            window["max_cc_t"].update(visible=compare_cc)
            window["max_cc"].update(visible=compare_cc)
            window["min_cc_t"].update(visible=compare_cc)
            window["min_cc"].update(visible=compare_cc)
            window["rand_per_t"].update(visible=not compare_cc)
            window["rand_per"].update(visible=not compare_cc)
            window["rand_per_rad"].update(visible=not compare_cc)
            window["cc_rad"].update(visible=not compare_cc)

        if compare_cc:
            if values["min_cc"] != "":
                min_cc = float(values["min_cc"])
            if values["max_cc"] != "":
                max_cc = float(values["max_cc"])

        compare_d = values["compare_d"]
        if not facebook:
            window["max_degree_t"].update(visible=compare_d)
            window["max_degree"].update(visible=compare_d)
            window["min_degree_t"].update(visible=compare_d)
            window["min_degree"].update(visible=compare_d)
            window["degree_t"].update(visible=not compare_d)
            window["degree"].update(visible=not compare_d)

        if compare_d:
            if values["min_degree"] != "":
                min_d = float(values["min_degree"])
            if values["max_degree"] != "":
                max_d = float(values["max_degree"])

        compare_dis = values["compare_dis"]
        if not facebook:
            vis1 = values["dis_1"] == "Beta distribution" and compare_dis
            vis2 = values["dis_2"] == "Beta distribution" and compare_dis
            whitelist1 = np.array(
                ["beta1", "alpha1", "alpha1_t", "beta1_t", "scale1", "scale1_t"]
            )
            whitelist2 = np.array(
                ["beta2", "alpha2", "alpha2_t", "beta2_t", "scale2", "scale2_t"]
            )
            window["dis_1"].update(visible=compare_dis)
            window["dis_2"].update(visible=compare_dis)
            window["dis"].update(visible=not compare_dis)
            for x in whitelist1:
                window[x].update(visible=vis1)
            for x in whitelist2:
                window[x].update(visible=vis2)

        if compare_dis:
            dis_1 = values["dis_1"]
            dis_2 = values["dis_2"]
            if values["alpha1"] != "":
                alpha1 = float(values["alpha1"])
            if values["beta1"] != "":
                beta1 = float(values["beta1"])
            if values["alpha2"] != "":
                alpha2 = float(values["alpha2"])
            if values["beta2"] != "":
                beta2 = float(values["beta2"])
            if values["scale1"] != "":
                scale1 = float(values["scale1"])
            if values["scale2"] != "":
                scale2 = float(values["scale2"])

        compare_face = values["compare_face"]
        blacklist = ["pop", "degree", "pop_t", "degree_t", "rand_per", "rand_per_t"]
        for elem in keys:
            if elem in blacklist:
                window[elem].update(visible=not compare_face)

        if values["rand_per_rad"]:
            cc_mod = 0
        elif values["cc_rad"]:
            cc_mod = 1
        if not facebook:
            window["rand_per"].update(visible=cc_mod == 0 and not compare_cc)
            window["rand_per_t"].update(visible=cc_mod == 0 and not compare_cc)
            window["cc"].update(visible=cc_mod == 1 and not compare_cc)
            window["cc_t"].update(visible=cc_mod == 1 and not compare_cc)

        show = not values["save"]
        if values["rand_per"] != "":
            rand_per = float(values["rand_per"])
        if values["pop"] != "":
            pop = int(values["pop"])
        if values["seed"] != "":
            seed = int(values["seed"])
        if values["degree"] != "":
            degree = float(values["degree"])
        if values["min_op_diff"] != "":
            min_op_diff = float(values["min_op_diff"])
        if values["cc"] != "":
            cc = float(values["cc"])
        distribution = values["dis"]

        n = "Beta distribution"
        vis = distribution == n and window["dis"].visible
        window["alpha_t"].update(visible=vis)
        window["alpha"].update(visible=vis)
        window["beta_t"].update(visible=vis)
        window["beta"].update(visible=vis)
        window["scale_t"].update(visible=vis)
        window["scale"].update(visible=vis)
        if vis and values["alpha"] != "":
            alpha = float(values["alpha"])
        if vis and values["beta"] != "":
            beta = float(values["beta"])
        if vis and values["scale"] != "":
            scale = float(values["scale"])

        # End program if user closes window or
        # presses the OK button
        if event == "OK" or event == sg.WIN_CLOSED:
            break

    window.close()
    return (
        show,
        cc_mod,
        compare_cc,
        max_cc,
        min_cc,
        compare_d,
        max_d,
        min_d,
        pop,
        rand_per,
        seed,
        degree,
        min_op_diff,
        op_lim,
        distribution,
        compare_dis,
        dis_1,
        dis_2,
        alpha,
        beta,
        alpha1,
        beta1,
        alpha2,
        beta2,
        scale,
        scale1,
        scale2,
        facebook,
        compare_face,
        cc,
    )


def modules_temp(
    Y_modules_temp,
    modules_num,
    nodes,
    timestep,
    face=False,
    whitelist=None,
):
    """
    Add the new average opinions per module value in array.
    Y_modules_temp: Array to add value to
    modules_num: number of modules
    nodes: array of all nodes
    timestep: at which time step to add the value to
    face: whether network is the facebook network
    whitelist: nodes to use for the facebook network
    """
    Y_modules_temp.append(np.zeros(modules_num, dtype=float))
    module_ops = np.zeros((modules_num, 2), dtype=float)
    for n in nodes:
        if not face or n["id"] in whitelist:
            module_ops[n["modul"]][0] += n["opinion"]
            module_ops[n["modul"]][1] += 1
    for m in range(modules_num):
        if module_ops[m][1] != 0:
            Y_modules_temp[timestep][m] = module_ops[m][0] / module_ops[m][1]
            if abs(Y_modules_temp[timestep][m]) > 3:
                test = 0
        else:
            Y_modules_temp[timestep][m] = 0


def op_abs_ave(Y_op_abs_ave, nodes, timestep, face=False, whitelist=None):
    """
    Add the new absolute average opinion value in array.
    Y_op_abs_ave: Array to add value to
    nodes: array of all nodes
    timestep: at which time step to add the value to
    face: whether network is the facebook network
    whitelist: nodes to use for the facebook network
    """
    Y_op_abs_ave.append(0)
    counter = 0
    for n in range(len(nodes)):
        if not face or n in whitelist:
            Y_op_abs_ave[timestep] += nodes[n]["opinion"]
            counter += 1
    Y_op_abs_ave[timestep] /= counter


def in_out_degree(
    Y_avg_in_degree,
    Y_avg_out_degree,
    nodes,
    G,
    face=False,
    whitelist=None,
):
    """
    Add the new average in and out degree of all modules in array.
    Y_avg_in_degree: Array to add value to
    Y_avg_out_degree: Array to add value to
    nodes: array of all nodes
    G: Graph
    timestep: at which time step to add the value to
    face: whether network is the facebook network
    whitelist: nodes to use for the facebook network
    """
    avg_out_degree = 0
    avg_in_degree = 0
    counter_deg = 0
    for n in range(len(nodes)):
        if not face or n in whitelist:
            if abs(nodes[n]["opinion"]) < 3:
                avg_out_degree += G.out_degree(n)
                avg_in_degree += G.in_degree(n)
                counter_deg += 1
    if counter_deg != 0:
        avg_out_degree /= counter_deg
        avg_in_degree /= counter_deg

    Y_avg_in_degree.append(avg_in_degree)
    Y_avg_out_degree.append(avg_out_degree)
