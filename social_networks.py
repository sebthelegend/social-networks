from tkinter.constants import TRUE
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib
from networkx.algorithms.connectivity.disjoint_paths import node_disjoint_paths
from networkx.classes.function import nodes_with_selfloops
import numpy as np
import random as rand
import networkx as nx
import networkx.drawing as nxd
import math
import os
import graph_building as gb
import tkinter as tk
from multiprocessing import Process, Queue
from mpl_toolkits.mplot3d import Axes3D
from networkx.algorithms import community
import collections

# seeds:
seeds = np.array([1029, 1013, 9999, 2000, 2001, 2002, 2003, 2004, 2005, 2006])

# initialisation of all variables
try:
    input = gb.make_gui()
except:
    print("Program stopped!")
    exit()
"""
    help for gui:
    - save: if graphs should be saved as pgfs or shown
    - import Facebook: import a Facebook network
    - Compare clustering Coefficient: compare 2 different clustering coefficients
    - Compare Density: compare 2 different average degrees
    - Compare Facebook: compare the Facebook network with a constructed network
    - Population: number of nodes
    - Random Percentage: Clustering coefficient will be modified by rewiring the percantage of edges (fast)
    - Clustering Coefficient: Will produce a network with the given Clustering Coefficient (slow)
    - Seed: seed for randomness
    - minimal opinion difference: Simulation stops when the opinion difference in a time step is below the given minimum
    - Distribution: Degree distribution

"""

show = input[0]
cc_mod = input[1]
compare_cc = input[2]
rand_max = input[3]
rand_min = input[4]
compare_d = input[5]
degree_max = input[6]
degree_min = input[7]
pop = input[8]
rand_per = input[9]
seednum = input[10]
degree = input[11]
min_opinion_diff = input[12]
max_op = input[13]
distribution = input[14]
compare_dis = input[15]
distribution_1 = input[16]
distribution_2 = input[17]
alpha = input[18]
beta = input[19]
alpha1 = input[20]
beta1 = input[21]
alpha2 = input[22]
beta2 = input[23]
scale = input[24]
scale1 = input[25]
scale2 = input[26]
facebook = input[27]
compare_face = input[28]
cc_aim = input[29]


comparison = compare_dis or compare_cc or compare_d or compare_face

low = rand_per == 1
face_whitelist = None
face = facebook or compare_face

if not show:
    matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "xelatex",
            "font.family": "serif",
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

if facebook or compare_face:
    face_whitelist = np.full(5411, -1)
    pop = 7057
    # pop = 5411
    compare_cc = False
    compare_d = False
    degree = 14
    rand_per = 0.21
    scale = -100
    cc_mod = 0

r = 2
if not comparison:
    r = 1


for big_loop in range(r):

    # comparisons
    if compare_cc:
        cc_mod = 0
        if big_loop == 0:
            rand_per = rand_max
        else:
            rand_per = rand_min
        low = rand_per == 1
        if low:
            os.chdir(os.getcwd() + "\\..")

    if compare_d:
        if big_loop == 0:
            degree = degree_min
        else:
            degree = degree_max

    if compare_face:
        if big_loop == 0:
            facebook = True
        else:
            facebook = False
            os.chdir(os.getcwd() + "\\..")
            pop = 5411

    if compare_dis:
        if big_loop == 0:
            distribution = distribution_1
            alpha = alpha1
            beta = beta1
            scale = scale1
        else:
            distribution = distribution_2
            alpha = alpha2
            beta = beta2
            scale = scale2

    # directory change for your own system
    current = os.getcwd()
    if facebook:
        os.chdir(current + "\\facebook")
    elif not low:
        os.chdir(current + "\\high")
    else:
        os.chdir(current + "\\low")

    # node attributes
    G = nx.DiGraph()
    G_undirect = nx.Graph()
    node_t = [
        ("opinion", float),
        ("social", float),
        ("y", float),
        ("x", float),
        ("id", int),
        ("modul", int),
        ("cluster", int),
        ("clustering", float),
    ]
    nodes = np.full(pop, max_op + 1, dtype=node_t)

    # gephi .gexf file
    f = open("graph" + str(seednum) + ".gexf", "w")
    f.write(
        '<gexf xmlns="http://www.gexf.net/1.3" version="1.3" xmlns:viz="http://www.gexf.net/1.3/viz" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.gexf.net/1.3 http://www.gexf.net/1.3/gexf.xsd">")\n'
    )
    f.close()
    f = open("graph" + str(seednum) + ".gexf", "a")
    edges = ""

    # creating/importing the network
    if facebook:
        nodes, G = gb.import_Facebook(G, nodes, max_op, seeds[seednum - 1])
        counter = 0
        for n in range(7057):
            if G.in_degree(n) != 0 and G.out_degree(n) != 0:
                face_whitelist[counter] = n
                counter += 1
    else:
        G, nodes = gb.build_G(
            G,
            nodes,
            max_op,
            distribution,
            seeds[seednum - 1],
            alpha=alpha,
            beta=beta,
            social_scale=scale,
        )
        G, show_neigh = gb.connect_G(nodes, G, rand_per, degree, cc_aim, cc_mod)

    print("Density: " + str(round(nx.density(G), 2)))

    print(
        "Assortativity:",
        round(nx.degree_assortativity_coefficient(G, x="out", y="in"), 2),
    )

    # all structural attributes
    # average shortest path: average of pop number of edges
    avg_path = 0
    for n in range(pop):
        found = False
        while not found:
            s = round(rand.random() * (pop - 1))
            t = round(rand.random() * (pop - 1))
            try:
                avg_path += nx.shortest_path_length(G, source=s, target=t)
                found = True
            except:
                DoNothing = True

    avg_path /= pop
    print("average shortest path:", avg_path)

    # degree deviation
    G_undirect = G.to_undirected()

    degree_sequence = sorted(
        [d for n, d in G.degree()], reverse=True
    )  # degree sequence
    degreeCount = collections.Counter(degree_sequence)

    counter = 0
    for n in degreeCount:
        x = degreeCount[n]
        counter += abs(x * (degree * 2 - n))
    print("degree deviation:", counter / pop)

    # clustering coefficient
    cc = 0
    pop = len(list(G))
    G_undirect = G.to_undirected()
    if compare_face and facebook:
        ccs = np.zeros(5411)
    else:
        ccs = np.zeros(pop)
    counter = 0
    for i in range(pop):
        if not facebook or i in face_whitelist:
            temp = gb.local_clustering(G, G_undirect, i)
            ccs[counter] = temp
            nodes[i]["clustering"] = temp
            G.nodes[i]["clustering"] = temp
            cc += temp
            counter += 1

    cc = round(cc / pop, 3)
    print("Clustering coefficient: " + str(cc))

    nx.write_graphml(G, "start" + str(seednum) + ".graphml")

    # clusters
    test_gen4 = community.asyn_lpa_communities(G_undirect)

    mods = list()
    modul = 0
    for t in test_gen4:
        mods.append(t)
    print("modules: " + str(len(mods)))
    modules_num = len(mods)
    if modules_num == 0:
        modules_num = 1
    modul = 0
    for t in mods:
        for n in t:
            G.nodes[n]["modul"] = modul
            nodes[n]["modul"] = modul
        modul += 1
    modul -= 1

    f.write(gb.spaces(2) + '<graph defaultedgetype="directed" mode="dynamic">\n')
    f.write(gb.spaces(4) + '<attributes class="node" mode="static">\n')
    f.write(
        gb.spaces(6)
        + '<attribute id="social" title="social" type="float"></attribute>\n'
    )
    f.write(gb.spaces(6) + '<attribute id="y" title="y" type="float"></attribute>\n')
    f.write(gb.spaces(6) + '<attribute id="x" title="x" type="float"></attribute>\n')
    f.write(
        gb.spaces(6) + '<attribute id="modul" title="modul" type="int"></attribute>\n'
    )
    f.write(
        gb.spaces(6)
        + '<attribute id="cluster" title="cluster" type="int"></attribute>\n'
    )
    f.write(
        gb.spaces(6)
        + '<attribute id="last_opinion" title="last_opinion" type="float"></attribute>\n'
    )
    f.write(
        gb.spaces(6)
        + '<attribute id="clustering" title="clustering" type="float"></attribute>\n'
    )
    f.write(
        gb.spaces(4)
        + "</attributes>\n"
        + gb.spaces(4)
        + '<attributes class="node" mode="dynamic">\n'
    )
    f.write(
        gb.spaces(6)
        + '<attribute id="opinion" title="opinion" type="float"></attribute>\n'
    )
    f.write(gb.spaces(4) + "</attributes>\n" + gb.spaces(4) + "<nodes>\n")

    node_op = np.empty(pop, dtype=list)
    for n in range(pop):
        node_op[n] = list()
        node_op[n].append(nodes[n]["opinion"])

    # initialize Y values for the graphs
    Z_temp = list()
    Y_modules_temp = list()
    Y_opinion_diff = list()
    Y_op_abs_ave = list()
    Y_pol = list()
    Y_pol_mod = list()
    Y_avg_out_degree = list()
    Y_avg_in_degree = list()

    # add first entry
    Y_pol.append(gb.polarization_op(nodes, face=face, whitelist=face_whitelist))

    Z_temp.append(np.array(np.zeros(pop, dtype=float)))
    graph = np.sort(nodes, order=["opinion", "modul"])
    for n in range(pop):
        if not facebook or n in face_whitelist:
            Z_temp[0][n] = graph[n][0]

    gb.modules_temp(
        Y_modules_temp,
        modules_num,
        nodes,
        0,
        face=face,
        whitelist=face_whitelist,
    )
    gb.op_abs_ave(Y_op_abs_ave, nodes, 0, face=face, whitelist=face_whitelist)
    gb.in_out_degree(
        Y_avg_in_degree,
        Y_avg_out_degree,
        nodes,
        G,
        face=face,
        whitelist=face_whitelist,
    )

    Y_opinion_diff.append(0)

    opinion_diff = min_opinion_diff + 1

    # main while-loop of simulation
    timestep = 0
    print("timestep:")
    diff = opinion_diff
    while diff > min_opinion_diff:
        print(timestep, end="\r")
        opinion_diff = 0

        # opinion formation model
        for n in G.nodes:
            op = nodes[n][0]
            for ne in nx.neighbors(G, n):
                G.nodes[ne]["opinion"] += 0.1 * op

                if G.nodes[ne]["opinion"] > 3:
                    G.nodes[ne]["opinion"] = 3
                if G.nodes[ne]["opinion"] < -3:
                    G.nodes[ne]["opinion"] = -3

        for n in range(pop):
            if not facebook or n in face_whitelist:
                opinion_diff += abs(nodes[n][0] - G.nodes[n]["opinion"])
            nodes[n]["opinion"] = G.nodes[n]["opinion"]
            node_op[n].append(nodes[n]["opinion"])

        timestep += 1
        diff = opinion_diff
        # append values of the timestep to the graphs
        Y_pol.append(gb.polarization_op(nodes, face=face, whitelist=face_whitelist))

        gb.in_out_degree(
            Y_avg_in_degree,
            Y_avg_out_degree,
            nodes,
            G,
            face=face,
            whitelist=face_whitelist,
        )
        gb.op_abs_ave(
            Y_op_abs_ave,
            nodes,
            timestep,
            face=face,
            whitelist=face_whitelist,
        )
        gb.modules_temp(
            Y_modules_temp,
            modules_num,
            nodes,
            timestep,
            face=face,
            whitelist=face_whitelist,
        )
        Y_opinion_diff.append(opinion_diff)

        Z_temp.append(np.array(np.zeros(pop, dtype=float)))
        graph = np.sort(nodes, order=["opinion", "modul"])
        for n in range(pop):
            Z_temp[timestep][n] = graph[n][0]
        y = -1
    print("")
    # save data if comparison
    if comparison and big_loop == 0:
        modules_num_h = modules_num
        timestep_h = timestep
        cc_h = cc
        Z_temp_h = Z_temp.copy()
        Y_modules_temp_h = Y_modules_temp.copy()
        Y_opinion_diff_h = Y_opinion_diff.copy()
        Y_op_abs_ave_h = Y_op_abs_ave.copy()
        Y_pol_h = Y_pol.copy()
        # Y_pol_mod_h = Y_pol_mod.copy()
        node_op_h = node_op.copy()
        Y_avg_out_degree_h = Y_avg_out_degree.copy()
        Y_avg_in_degree_h = Y_avg_in_degree.copy()
        G_h = G.copy()
        ccs_h = ccs.copy()

    for n in G.nodes:
        f.write(gb.spaces(6) + '<node id="' + str(n) + '" label="' + str(n) + '">\n')
        f.write(gb.spaces(8) + "<attvalues>\n")
        f.write(
            gb.spaces(10)
            + '<attvalue for="social" value="'
            + str(G.nodes[n]["social"])
            + '"/>\n'
        )
        f.write(
            gb.spaces(10) + '<attvalue for="y" value="' + str(G.nodes[n]["y"]) + '"/>\n'
        )
        f.write(
            gb.spaces(10) + '<attvalue for="x" value="' + str(G.nodes[n]["x"]) + '"/>\n'
        )
        f.write(
            gb.spaces(10)
            + '<attvalue for="modul" value="'
            + str(G.nodes[n]["modul"])
            + '"/>\n'
        )
        f.write(
            gb.spaces(10)
            + '<attvalue for="cluster" value="'
            + str(G.nodes[n]["cluster"])
            + '"/>\n'
        )
        f.write(
            gb.spaces(10)
            + '<attvalue for="last_opinion" value="'
            + str(G.nodes[n]["opinion"])
            + '"/>\n'
        )
        f.write(
            gb.spaces(10)
            + '<attvalue for="clustering" value="'
            + str(G.nodes[n]["clustering"])
            + '"/>\n'
        )
        for t in range(timestep):
            f.write(
                gb.spaces(10)
                + '<attvalue for="opinion" value="'
                + str(node_op[n][t])
                + '" start="'
                + str(t)
                + '.0" end="'
                + str(t + 1)
                + '.0"/>\n'
            )
        f.write(gb.spaces(8) + "</attvalues>\n" + gb.spaces(6) + "</node>\n")

    f.write(gb.spaces(4) + "</nodes>\n" + gb.spaces(4) + "<edges>\n")

    count = 0
    for x in range(pop):
        if facebook:
            for ne in list(G[x]):
                f.write(
                    gb.spaces(6)
                    + '<edge source="'
                    + str(x)
                    + '" target="'
                    + str(ne)
                    + '"/>\n'
                )
                count += 1
        else:
            for ne in show_neigh[x]:
                f.write(
                    gb.spaces(6)
                    + '<edge source="'
                    + str(x)
                    + '" target="'
                    + str(ne)
                    + '"/>\n'
                )

    f.write(gb.spaces(4) + "</edges>\n" + gb.spaces(2) + "</graph>\n</gexf>")
    f.close()
    nx.write_graphml(G, "end" + str(seednum) + ".graphml")

# edges in and out of clusters
edges_in = 0
edges_out = 0
for m in mods:
    for c in m:
        temp_e_i = G.in_degree(c)
        temp_e_o = 0
        for neigh in list(G_undirect[c]):
            if neigh in m and neigh in list(G[c]):
                edges_in += 1
            if not neigh in m and c in list(G[neigh]):
                edges_out += 1
                temp_e_o += 1
            if (temp_e_o * 2) > temp_e_i:
                counter += 1
                # print("In: " + str(temp_e_i) + " Out: " + str(temp_e_o))
print("edges out: " + str(edges_out) + "\nedges in: " + str(edges_in))
print("Counter: " + str(counter))

# initialising graphs
plt.style.use("seaborn-whitegrid")
figs = list()
font_s = 10
tfont_s = 10
dis = 10

identi = ""
label_l = ""
label_h = ""
dis_name = distribution.rsplit(" ")
alpha_name = "$\\alpha$"
beta_name = "$\\beta$"
if dis_name[0] == "Beta":
    dis_name[0] += "_" + str(alpha) + "_" + str(beta)
if show:
    alpha_name = "\u03B1"
    beta_name = "\u03B2"

if compare_cc:
    label_h = "Clustering coefficient: " + str(cc_h)
    label_l = "Clustering coefficient: " + str(cc)
    identi = "compare_cc_" + dis_name[0]
elif compare_d:
    label_h = "Density: " + str(round(degree_min / pop, 2))
    label_l = "Density: " + str(round(degree_max / pop, 2))
    identi = "compare_d_" + dis_name[0]
elif compare_dis:
    label_h = "Distribution: " + distribution_1
    if distribution_1 == "Beta distribution":
        label_h += (
            alpha_name
            + ": "
            + str(alpha1)
            + "; "
            + beta_name
            + ": "
            + str(beta1)
            + "; Social scale: "
            + str(scale1)
        )
    label_l = "Distribution: " + distribution_2
    if distribution_2 == "Beta distribution":
        label_l += (
            alpha_name
            + ": "
            + str(alpha2)
            + "; "
            + beta_name
            + ": "
            + str(beta2)
            + "; Social scale: "
            + str(scale2)
        )
    identi = "compare_dis_"
elif compare_face:
    label_h = "facebook"
    label_l = "constructed network"
    identi = "compare_facebook_"
elif facebook:
    identi = "facebook" + dis_name[0] + "_"
elif not low:
    identi = "high_" + dis_name[0] + "_"
else:
    identi = "low_" + dis_name[0] + "_"
if not show:
    height = 2
    width = 3
    # add your Path in which all Figures will be saved
    path = current + "\\Figures\\"

density = round(nx.density(G), 2)

# graph for degree distribution


fig = plt.figure()
if comparison:
    # plt_h = fig.add_subplot(121)
    # plt_l = fig.add_subplot(122)
    degree_sequence_h = sorted(
        [d for n, d in G_h.degree()], reverse=True
    )  # degree sequence
    degreeCount_h = collections.Counter(degree_sequence_h)
    deg_h, cnt_h = zip(*degreeCount_h.items())

    degree_sequence = sorted(
        [d for n, d in G.degree()], reverse=True
    )  # degree sequence
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())
    x = np.array(deg)
    x_h = np.array(deg_h)
    w = 0.5
    plt.bar(x_h - w / 2, cnt_h, width=w, color="b", label=label_h)
    plt.bar(x + w / 2, cnt, width=w, color="r", label=label_l)
    plt.legend(prop={"size": font_s})
else:
    degree_sequence = sorted(
        [d for n, d in G.degree()], reverse=True
    )  # degree sequence
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())

    plt.bar(deg, cnt, width=0.80, color="b")
plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.ylabel("number", size=font_s)
plt.xlabel("degree", size=font_s)
if not show:
    width, height = gb.set_size()
    fig.set_size_inches(w=width, h=height)
    name = "degree_dist" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for module opinion
fig = plt.figure()
cw = cm.get_cmap("coolwarm", 61)
palet = cw(np.linspace(0, 1, 61))
Y_module = np.empty((modules_num, timestep + 1))
if comparison:
    plt_h = fig.add_subplot(121)
    plt_l = fig.add_subplot(122)
    Y_module_h = np.empty((modules_num_h, timestep_h + 1))

    for x in range(modules_num):
        for w in range(timestep + 1):
            Y_module[x][w] = Y_modules_temp[w][x]
        c = palet[int((round(Y_module[x][-1], 1) + 3) * 10)]
        plt_l.plot(Y_module[x], color=c)

    for x in range(modules_num_h):
        for w in range(timestep_h + 1):
            Y_module_h[x][w] = Y_modules_temp_h[w][x]
        c = palet[int((round(Y_module_h[x][-1], 1) + 3) * 10)]
        plt_h.plot(Y_module_h[x], color=c)
else:
    for x in range(modules_num):
        for w in range(timestep + 1):
            Y_module[x][w] = Y_modules_temp[w][x]
        c = palet[int((round(Y_module[x][-1], 1) + 3) * 10)]
        plt.plot(Y_module[x], color=c)

if comparison:
    plt_h.set_title(label_h, pad=dis, loc="left", size=tfont_s)
    plt_l.set_title(label_l, pad=dis, loc="left", size=tfont_s)
    plt_h.set_ylabel("Opinion", size=font_s)
    plt_h.set_xlabel("Time", size=font_s)
    plt_l.set_xlabel("Time", size=font_s)
else:
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)
    plt.ylabel("Opinion", size=font_s)
    plt.xlabel("Time", size=font_s)

if not show:
    width, height = gb.set_size()
    fig.set_size_inches(w=width, h=height)
    name = "opinion_modules" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)


# Graph for polarization (opinion)
figs.append(plt.figure())
if comparison:
    plt.plot(Y_pol_h, color="red", label=label_h)
    plt.plot(Y_pol, color="blue", linestyle="dashed", label=label_l)
    plt.legend(prop={"size": font_s})
else:
    plt.plot(Y_pol)
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.ylabel("Polarization (opinion)", size=font_s)
plt.xlabel("Time", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "polarization_op" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)


# graph for opinion difference
figs.append(plt.figure())
if comparison:
    plt.plot(Y_opinion_diff_h, label=label_h, color="red")
    plt.plot(Y_opinion_diff, label=label_l, color="blue", linestyle="dashed")
    plt.legend(prop={"size": font_s})
else:
    plt.plot(Y_opinion_diff, label="Opinion Difference")
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.ylabel("Opinion Difference", size=font_s)
plt.xlabel("Time", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "opinion_difference" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for avg_out degree
figs.append(plt.figure())
if comparison:
    plt.plot(Y_avg_out_degree_h, label=label_h, color="red")
    plt.plot(Y_avg_out_degree, label=label_l, color="blue", linestyle="dashed")
    plt.legend(prop={"size": font_s})
else:
    plt.plot(Y_avg_out_degree, label="Average out-Degree")
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.ylabel("Average out-Degree", size=font_s)
plt.xlabel("Time", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "avg_out_degree" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for avg_in degree
figs.append(plt.figure())
if comparison:
    plt.plot(Y_avg_in_degree_h, label=label_h, color="red")
    plt.plot(Y_avg_in_degree, label=label_l, color="blue", linestyle="dashed")
    plt.legend(prop={"size": font_s})
else:
    plt.plot(Y_avg_in_degree, label="Average in-Degree")
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.ylabel("Average in-Degree", size=font_s)
plt.xlabel("Time", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "avg_in_degree" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for average absolute
figs.append(plt.figure())
if comparison:
    plt.plot(Y_op_abs_ave_h, label=label_h, color="red")
    plt.plot(Y_op_abs_ave, label=label_l, color="blue", linestyle="dashed")
else:
    plt.plot(Y_op_abs_ave)
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.legend(prop={"size": font_s})
plt.xlabel("Time", size=font_s)
plt.ylabel("average absolute opinion", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "opinion_average" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)


# Graph for number of opinions

x = np.linspace(0, timestep, timestep + 1)
figs.append(plt.figure())
if comparison:
    plt_h = figs[-1].add_subplot(211)
    plt_l = figs[-1].add_subplot(212)
    x_h = np.linspace(0, timestep_h, timestep_h + 1)
    if compare_face:
        node_op_h_temp = np.empty((5411, timestep_h + 1), dtype=float)
    else:
        node_op_h_temp = np.empty((pop, timestep_h + 1), dtype=float)
    node_op_temp = np.empty((pop, timestep + 1), dtype=float)

    if compare_face:
        counter = 0
        for n in range(7057):
            if n in face_whitelist:
                for t in range(timestep_h + 1):
                    node_op_h_temp[counter][t] = node_op_h[n][t]
                counter += 1
        for n in range(pop):
            for t in range(timestep + 1):
                node_op_temp[n][t] = node_op[n][t]
    else:
        for n in range(pop):
            for t in range(timestep_h + 1):
                node_op_h_temp[n][t] = node_op_h[n][t]
            for t in range(timestep + 1):
                node_op_temp[n][t] = node_op[n][t]

    # node_op = np.abs(node_op_temp)
    # node_op_h = np.abs(node_op_h_temp)
    node_op = node_op_temp
    node_op_h = node_op_h_temp
    np.around(node_op_h, decimals=1, out=node_op_h)
    op_counter_h = np.zeros((61, timestep_h + 1), dtype=int)
    np.around(node_op, decimals=1, out=node_op)
    op_counter = np.zeros((61, timestep + 1), dtype=int)
    for t in range(timestep + 1):
        for n in range(pop):
            for i in range(int(node_op[n][t] * 10) + 31):
                op_counter[i][t] += 1
    for t in range(timestep_h + 1):
        for n in range(len(node_op_h)):
            for i in range(int(node_op_h[n][t] * 10) + 31):
                op_counter_h[i][t] += 1
    palet = cw(np.linspace(0, 1, 61))
    for i in range(61):
        plt_l.plot(op_counter[i], color=palet[i])
        plt_h.plot(op_counter_h[i], color=palet[i])
    for i in range(60):
        plt_l.fill_between(x, op_counter[i], op_counter[i + 1], color=palet[i])
        plt_h.fill_between(x_h, op_counter_h[i], op_counter_h[i + 1], color=palet[i])
    plt_l.fill_between(x, op_counter[60], np.zeros(timestep + 1), color=palet[60])
    plt_h.fill_between(x_h, op_counter_h[60], np.zeros(timestep_h + 1), color=palet[60])
    norm = cm.colors.Normalize(vmin=-3, vmax=3)
    cbar_ax = figs[-1].add_axes([0.91, 0.15, 0.05, 0.7])
    figs[-1].colorbar(cm.ScalarMappable(norm=norm, cmap=cw), cax=cbar_ax)

else:
    if facebook:
        node_op_temp = np.empty((5411, timestep + 1), dtype=float)
    else:
        node_op_temp = np.empty((pop, timestep + 1), dtype=float)
    counter = 0
    for n in range(pop):
        if not facebook or n in face_whitelist:
            for t in range(timestep + 1):
                node_op_temp[counter][t] = node_op[n][t]
            counter += 1

    node_op = node_op_temp
    np.around(node_op, decimals=1, out=node_op)
    op_counter = np.zeros((61, timestep + 1), dtype=int)
    for t in range(timestep + 1):
        for n in range(len(node_op)):
            for i in range(int(node_op[n][t] * 10) + 31):
                op_counter[i][t] += 1
    palet = cw(np.linspace(0, 1, 61))
    for i in range(61):
        plt.plot(op_counter[i], color=palet[i])
    for i in range(60):
        plt.fill_between(x, op_counter[i], op_counter[i + 1], color=palet[i])
    plt.fill_between(x, op_counter[60], np.zeros(timestep + 1), color=palet[60])
    norm = cm.colors.Normalize(vmin=-3, vmax=3)
    figs[-1].colorbar(cm.ScalarMappable(norm=norm, cmap=cw))

if comparison:
    plt_h.set_title(label_h, pad=dis, loc="left", size=tfont_s)
    plt_l.set_title(label_l, pad=dis, loc="left", size=tfont_s)
    plt_h.legend(prop={"size": font_s})
    plt_l.legend(prop={"size": font_s})
    plt_h.set_ylabel("Number", size=font_s)
    plt_h.set_xlabel("Time", size=font_s)
    plt_l.set_xlabel("Time", size=font_s)
else:
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)
    plt.legend(prop={"size": font_s})
    plt.ylabel("Number", size=font_s)
    plt.xlabel("Time", size=font_s)
if not show:
    if comparison:
        width, height = gb.set_size(subplots=(2, 1))
    else:
        width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "opinion_number" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for clustering coefficient distribution
figs.append(plt.figure())
x = np.linspace(0, 1, 1001)
if comparison:
    ccs_num = np.zeros(1001)
    for c in ccs:
        ccs_num[round(c * 1000)] += 1

    ccs_num_h = np.zeros(1001)
    for c in ccs_h:
        ccs_num_h[round(c * 1000)] += 1

    plt.plot(x, ccs_num, label=label_l, color="red")
    plt.plot(x, ccs_num_h, label=label_h, color="blue")

else:
    ccs_num = np.zeros(1001)
    for cc in ccs:
        ccs_num[round(cc * 1000)] += 1
    plt.plot(x, ccs_num, color="red")
    plt.title("Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s)

plt.legend(prop={"size": font_s})
plt.xlabel("Clustering coefficient", size=font_s)
plt.ylabel("Number", size=font_s)
if not show:
    width, height = gb.set_size()
    figs[-1].set_size_inches(w=width, h=height)
    name = "ccs_num_" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)

# graph for ordered opinions of all nodes
fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

X = np.empty((pop, timestep + 1), dtype=int)
Y = np.empty_like(X, dtype=int)
Z = np.empty_like(X, dtype=float)
for x in range(pop):
    X[x] = np.arange(timestep + 1, dtype=int)
    Y[x] = np.array(np.full((1, 1), x), dtype=int)
    for w in range(timestep + 1):
        Z[x][w] = Z_temp[w][x]
if comparison:
    X_h = np.empty((pop, timestep_h + 1), dtype=int)
    Y_h = np.empty_like(X_h, dtype=int)
    Z_h = np.empty_like(X_h, dtype=float)
    for x in range(pop):
        X_h[x] = np.arange(timestep_h + 1, dtype=int)
        Y_h[x] = np.array(np.full((1, 1), x), dtype=int)
        for w in range(timestep_h + 1):
            Z_h[x][w] = Z_temp_h[w][x]

if comparison:
    plt_h = fig.add_subplot(121, projection="3d")
    plt_l = fig.add_subplot(122, projection="3d")
    plt_h.plot_surface(X_h, Y_h, Z_h, cmap=cm.coolwarm, linewidth=0)
    plt_l.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0)
    plt_h.set_title(
        "Clustering coefficient: " + str(cc_h), pad=dis, loc="left", size=tfont_s
    )
    plt_l.set_title(
        "Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s
    )
    plt_h.set_zlabel("Opinion", labelpad=dis, size=font_s)
    plt_h.set_xlabel("Time", labelpad=dis, size=font_s)
    plt_h.set_ylabel("Actors", labelpad=dis, size=font_s)
    plt_l.set_zlabel("Opinion", labelpad=dis, size=font_s)
    plt_l.set_xlabel("Time", labelpad=dis, size=font_s)
    plt_l.set_ylabel("Actors", labelpad=dis, size=font_s)
else:
    ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0)
    ax.set_title(
        "Clustering coefficient: " + str(cc), pad=dis, loc="left", size=tfont_s
    )
    ax.set_zlabel("Opinion", labelpad=dis, size=font_s)
    ax.set_xlabel("Time", labelpad=dis, size=font_s)
    ax.set_ylabel("Actors", labelpad=dis, size=font_s)
if show:
    plt.show()
else:
    width, height = gb.set_size(subplots=(1, 2))
    figs[-1].set_size_inches(w=width, h=height)
    name = "sorted_opinion" + str(seednum) + ".pgf"
    plt.savefig(path + identi + name)
