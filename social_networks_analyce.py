import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib
import numpy as np
import random as rand
import networkx as nx
import networkx.drawing as nxd
import math
import os
import collections
from scipy.stats import distributions
import graph_building as gb
from multiprocessing import Process, Queue, RawArray, Pool
from mpl_toolkits.mplot3d import Axes3D
from networkx.algorithms import community

"""
    show: show graphs or save them as pgfs
    calc: calculate new data (takes time) or take saved data
    save: save data in txt 
"""
show = True
calc = False
save = True

# modes:
# 0: cluster coefficient
# 1: degree
# 2: both degree and cc
# 3: distribution
# 4: social range
mode = 4

if not calc:
    save = True

if not show:
    matplotlib.use("pgf")
    matplotlib.rcParams.update(
        {
            "pgf.texsystem": "xelatex",
            "font.family": "serif",
            "text.usetex": True,
            "pgf.rcfonts": False,
        }
    )

pop = 1000


# attributes
max_degree = 50
min_degree = 5
static_rand_per = 0
seed_num = 1029
degree = 20
modules_num = 10
min_opinion_diff = 1
max_op = 3
num = 10
num_d = 10
if mode != 2:
    num_d = 0

# distributions:
# distribution = "Beta distribution"
# distribution = "Poisson distribution"
distribution = "Powerlaw"

if mode == 3:
    distribution = "Beta distribution"

    num = 0
    alphas = np.array([0.5, 0.5, 1, 1.5, 2, 2, 2, 2, 2, 2, 2, 2])
    betas = np.array([0.5, 1, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6])
    # alphas = np.array([1, 1])
    # betas = np.array([2, 3])
    num_dis = len(alphas)
elif mode == 4:
    num = 0
    distribution = "Beta distribution"
    num_dis = 11
    soc_ranges = np.zeros(num_dis)
    scales = np.linspace(1, 60, num_dis)

    for i in range(0, num_dis):
        soc_ranges[i] = degree / (scales[i] * math.pi)
        print(str(soc_ranges[i]))
else:
    num_dis = 1

rand.seed(seed_num)
seeds = np.zeros(48)
for s in range(len(seeds)):
    seeds[s] = rand.random() * 10000

# add your path in which all saved simulations are.
current = os.getcwd()
os.chdir(current + "\\saved_simulations")
seeds = np.array([1013])

# worker method
def sim(seed):
    print("seed: " + str(seed))
    G = nx.DiGraph()
    G_orig = nx.DiGraph()
    node_t = [
        ("opinion", float),
        ("social", float),
        ("y", float),
        ("x", float),
        ("id", int),
        ("modul", int),
        ("cluster", int),
        ("clustering", float),
    ]
    nodes_orig = np.zeros(pop, dtype=node_t)
    # building network
    if mode != 3 or mode != 4:
        G_orig, nodes_orig = gb.build_G(G_orig, nodes_orig, max_op, distribution, seed)
    # intitialising data arrays
    X_ac_local = np.zeros((num + 1, num_d + 1, num_dis), dtype=float)
    Y_ac_local = np.zeros_like(X_ac_local)
    Y_ad_local = np.zeros_like(X_ac_local)
    Y_ts_local = np.zeros_like(X_ac_local)
    Y_m_local = np.zeros_like(X_ac_local)
    Y_op_local = np.zeros_like(X_ac_local)
    Y_max_opd_local = np.zeros_like(X_ac_local)

    Y_pol_local = np.zeros_like(X_ac_local)
    Y_mod_indi_local = np.zeros_like(X_ac_local)
    Y_num_max_opd_local = np.zeros_like(X_ac_local)
    Y_op_diff_max_local = np.zeros_like(X_ac_local)
    Y_edges_out_local = np.zeros_like(X_ac_local)
    Y_degree_spread = np.zeros_like(X_ac_local)
    viridis = cm.get_cmap("coolwarm", num + 1)
    palet = viridis(np.linspace(0, 1, num + 1))

    # calculating rand_per to evenly distribute the clustering coefficient
    if mode == 0 or mode == 2:
        acs = np.empty(11, dtype=float)
        for d in range(11):
            rand_per = (10 - d) / 10
            nodes = nodes_orig.copy()
            G = G_orig.copy()
            G, trash = gb.connect_G(nodes, G, rand_per, degree, 0, 0)
            all_neighbors = np.full(pop, list)
            for n in range(pop):
                all_neighbors[n] = list(G[n]).copy()

            for n in range(pop):
                for ne in list(G[n]):
                    if not n in all_neighbors[ne]:
                        all_neighbors[ne].append(n)

            ac = 0
            for i in range(pop):
                nbrs = all_neighbors[i]
                if len(nbrs) > 1:
                    edges = len(nx.subgraph(G, nbrs).edges)
                    ac += edges / (len(nbrs) * (len(nbrs) - 1))
            acs[d] = ac / pop

        step_w = (acs[-1] - acs[0]) / num
        rands = np.empty(num + 1, dtype=float)
        for x in range(num + 1):
            up = 0
            y = (x * step_w) + acs[0]
            for t in range(11):
                if acs[t] > y:
                    up = t
                    break
            slope = (acs[up] - acs[up - 1]) / 0.1
            rands[x] = 0.1 * (up - 1) + (y - acs[up - 1]) / slope
        rands[-1] = 1
    elif mode == 1:
        degrees = np.linspace(min_degree, max_degree, num + 1)
    if mode == 2:
        degrees = np.linspace(min_degree, max_degree, num_d + 1)

    # main loops for all different network-runs
    for c in range(num_d + 1):
        for d in range(num + 1):
            for dis in range(num_dis):
                if mode != 3 or mode != 4:
                    G = G_orig.copy()
                    nodes = nodes_orig.copy()
                if mode == 0:
                    G, trash = gb.connect_G(nodes, G, 1 - rands[d], degree, 0, 0)
                elif mode == 1:
                    G, trash = gb.connect_G(nodes, G, static_rand_per, degrees[d], 0, 0)
                elif mode == 2:
                    G, trash = gb.connect_G(nodes, G, 1 - rands[d], degrees[c], 0, 0)
                elif mode == 3:
                    G.clear()
                    nodes = np.zeros(pop, dtype=node_t)
                    G, nodes = gb.build_G(
                        G,
                        nodes,
                        max_op,
                        distribution,
                        seed,
                        alpha=alphas[dis],
                        beta=betas[dis],
                    )
                    G, trash = gb.connect_G(nodes, G, static_rand_per, degree, 0, 0)
                elif mode == 4:
                    G.clear()
                    nodes = np.zeros(pop, dtype=node_t)
                    G, nodes = gb.build_G(
                        G,
                        nodes,
                        max_op,
                        distribution,
                        seed,
                        social_scale=soc_ranges[dis],
                    )
                    G, trash = gb.connect_G(nodes, G, static_rand_per, degree, 0, 0)

                G_uni = G.to_undirected()

                # saving data
                all_neighbors = np.full(pop, list)
                for n in range(pop):
                    all_neighbors[n] = list(G[n]).copy()

                for n in range(pop):
                    for ne in list(G[n]):
                        if not n in all_neighbors[ne]:
                            all_neighbors[ne].append(n)

                degree_sequence = sorted(
                    [d for n, d in G.degree()], reverse=True
                )  # degree sequence
                degreeCount = collections.Counter(degree_sequence)

                for n in degreeCount:
                    x = degreeCount[n]
                    Y_degree_spread[d][c][dis] += abs(x * (degree * 2 - n))
                Y_degree_spread[d][c][dis] /= pop

                ac = 0
                for i in range(pop):
                    nbrs = all_neighbors[i]
                    if len(nbrs) > 1:
                        edges = len(nx.subgraph(G, nbrs).edges)
                        ac += edges / (len(nbrs) * (len(nbrs) - 1))
                ac = ac / pop
                Y_ac_local[d][c][dis] = ac
                Y_ad_local[d][c][dis] = nx.density(G)

                try:
                    test_gen4 = community.asyn_lpa_communities(G_uni)

                    test = list()
                    modul = 0
                    for t in test_gen4:
                        test.append(t)

                    modules_num = len(test)
                    if modules_num == 0:
                        modules_num = 1
                    modul = 0
                    for t in test:
                        for n in t:
                            G.nodes[n]["modul"] = modul
                            nodes[n]["modul"] = modul
                        modul += 1
                    modul -= 1
                    counter_in = 0
                    counter_out = 0
                    for t in test:
                        for n in t:
                            for neigh in all_neighbors[n]:
                                if neigh in t:
                                    counter_in += 1
                                else:
                                    counter_out += 1
                    Y_mod_indi_local[d][c][dis] = counter_in
                    Y_edges_out_local[d][c][dis] = counter_out

                except:
                    modules_num = 1

                opinion_diff = min_opinion_diff + 1
                max_op_diff = 0
                local_maximum = 0
                old_op_diff = -1
                timestep = 0
                while opinion_diff > min_opinion_diff:
                    opinion_diff = 0

                    for n in G.nodes:
                        op = nodes[n][0]
                        for ne in nx.neighbors(G, n):
                            G.nodes[ne]["opinion"] += 0.1 * op
                            if G.nodes[ne]["opinion"] > 3:
                                G.nodes[ne]["opinion"] = 3
                            if G.nodes[ne]["opinion"] < -3:
                                G.nodes[ne]["opinion"] = -3

                    for n in range(pop):
                        opinion_diff += abs(nodes[n][0] - G.nodes[n]["opinion"])
                        nodes[n][0] = G.nodes[n]["opinion"]
                    timestep += 1
                    if opinion_diff > max_op_diff:
                        max_op_diff = opinion_diff
                        Y_max_opd_local[d][c][dis] = timestep
                    if opinion_diff >= old_op_diff:
                        local_maximum = 1
                    elif local_maximum == 1 and timestep < 10:
                        Y_num_max_opd_local[d][c][dis] += 1

                        local_maximum = 0
                    old_op_diff = opinion_diff

                op_sum = 0
                for n in G.nodes:
                    op_sum += nodes[n]["opinion"]

                Y_op_local[d][c][dis] = abs(op_sum) / pop
                Y_m_local[d][c][dis] = modules_num
                Y_ts_local[d][c][dis] = timestep
                Y_pol_local[d][c][dis] = gb.polarization_op(nodes)
                Y_op_diff_max_local[d][c][dis] = max_op_diff

    return (
        Y_ac_local,
        Y_ad_local,
        Y_ts_local,
        Y_m_local,
        Y_op_local,
        Y_max_opd_local,
        Y_pol_local,
        Y_mod_indi_local,
        Y_op_diff_max_local,
        Y_edges_out_local,
        Y_degree_spread,
    )


# main method
if __name__ == "__main__":
    if calc:
        # initiating all workers
        with Pool(processes=len(seeds)) as pool:
            Ys = np.empty((11, len(seeds), num + 1, num_d + 1, num_dis), dtype=float)
            t = pool.map(sim, seeds)

            for s in range(len(seeds)):
                for y in range(len(Ys)):
                    Ys[y][s] = t[s][y]
        Ys = np.average(Ys, axis=1)
        # saving data in txt
        if save:
            if mode == 0:
                m = "cc_" + str(degree)
            elif mode == 1:
                m = "d2_" + str(static_rand_per)
            elif mode == 2:
                m = "cc_d"
            elif mode == 3:
                m = "dis_" + str(static_rand_per) + "_" + str(degree) + "_"
            elif mode == 4:
                m = "soc_"
            dist_name = distribution.rsplit(" ")
            f = open("save_" + m + "_" + dist_name[0] + ".txt", "w")
        else:
            f = open("temp.txt", "w")
        for i in range(num + 1):
            for c in range(num_d + 1):
                for d in range(num_dis):
                    for y in range(len(Ys)):
                        if mode == 3 or mode == 4 or y != 10:
                            f.write(str(Ys[y][i][c][d]) + "\n")
        f.close()
    if mode == 0 or mode == 1:
        Ys = np.empty((10, num + 1), dtype=float)
    elif mode == 2:
        Ys = np.empty((10, num + 1, num_d + 1), dtype=float)
    elif mode == 3 or mode == 4:
        Ys = np.empty((11, num_dis), dtype=float)
    # reading data out of txt
    if save:
        if mode == 0:
            m = "cc_" + str(degree)
        elif mode == 1:
            m = "d2_" + str(static_rand_per)
        elif mode == 2:
            m = "cc_d"
        elif mode == 3:
            m = "dis_" + str(static_rand_per) + "_" + str(degree) + "_"
        elif mode == 4:
            m = "soc_"
        dist_name = distribution.rsplit(" ")
        f = open("save_" + m + "_" + dist_name[0] + ".txt", "r")
    else:
        f = open("temp.txt", "r")
    for i in range(num + 1):
        for c in range(num_d + 1):
            for d in range(num_dis):
                for y in range(len(Ys)):
                    line = f.readline()
                    if line != "":
                        if mode == 2:
                            Ys[y][i][c] = float(line)
                        elif mode == 3 or mode == 4:
                            Ys[y][d] = float(line)
                        else:
                            Ys[y][i] = float(line)
    f.close()
    dist_name = distribution.rsplit(" ")
    # add your path in which all Figures should be saved
    path_basis = current + "\\Figures\\"

    # perparing for visualization
    if mode == 0:
        path = path_basis + dist_name[0] + "_"
    elif mode == 1:
        path = path_basis + "d_" + dist_name[0] + "_"
    elif mode == 2:
        path = path_basis + "d_cc_" + dist_name[0] + "_"
    elif mode == 3:
        path = path_basis + "dis_" + dist_name[0] + "_"
    elif mode == 4:
        path = path_basis + "soc_"
    width, height = gb.set_size()
    names = np.array(
        [
            "cc",
            "density",
            "time_num",
            "mod_num",
            "abs_op",
            "t_max_op_diff",
            "pol",
            "edges_in",
            "max_op_diff",
            "edges_out",
            "degree_spread",
        ]
    )
    labels = np.array(
        [
            "cc",
            "Density",
            "timesteps",
            "Modules",
            "opinion",
            "time of max opinion difference",
            "polarization",
            "edges intern",
            "max opinion difference",
            "edges extern",
            "Average spread of Degree",
        ]
    )
    if mode == 0:
        X = Ys[0]
    elif mode == 1:
        X = np.linspace(min_degree, max_degree, num + 1)

    elif mode == 2:
        Y = np.linspace(min_degree, max_degree, num_d + 1)

        X = np.empty(num + 1, dtype=float)
        for x in range(num + 1):
            X[x] = Ys[0][x][0]

        Y, X = np.meshgrid(Y, X)
    elif mode == 3:
        X = np.arange(num_dis)
    elif mode == 4:
        X = scales
    plt.style.use("seaborn-whitegrid")

    # building all graphs
    for y in range(len(Ys)):
        fig, ax = plt.subplots()
        plt.plot(X, Ys[y], "o-")
        plt.ylabel(labels[y])
        if mode == 0:
            plt.xlabel("Clustering Coefficient")
            plt.title("Average Degree: " + str(degree) + " Population: " + str(pop))
        elif mode == 1:
            plt.xlabel("degree")
            plt.title(
                "Clustering Coefficient: "
                + str(round(np.average(Ys[0]), 2))
                + " Population: "
                + str(pop)
            )
        elif mode == 3:
            loc = np.arange(len(alphas))
            ax.set_xticks(loc)
            ax.set_xticklabels(list(zip(alphas, betas)))
            for tick in ax.xaxis.get_major_ticks()[1::2]:
                tick.set_pad(15)
            plt.title(
                "Clustering Coefficient: "
                + str(round(np.average(Ys[0]), 2))
                + " Population: "
                + str(pop)
                + " Average Degree: "
                + str(degree)
            )
        elif mode == 4:
            plt.xlabel("social range")
            plt.title(
                "Clustering Coefficient: "
                + str(round(np.average(Ys[0]), 2))
                + " Population: "
                + str(pop)
                + " Average Degree: "
                + str(degree)
            )

        else:
            fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
            ax.plot_surface(X, Y, Ys[y], cmap=cm.coolwarm, linewidth=0)
            ax.set_title(labels[y])
        plt.legend(prop={"size": 17})
        fig.set_size_inches(w=width, h=height)
        if not show:
            plt.savefig(path + "an2_" + names[y] + ".pgf")

    if mode == 2:
        fig = plt.figure()
        Y = np.zeros(num_d + 1, dtype=float)
        pol = Ys[6]
        for y in range(num_d + 1):
            for x in range(num + 1):
                if pol[x][y] > 0.5:
                    Y[y] = Ys[0][x][y]
                    break
        X = np.linspace(min_degree, max_degree, num_d + 1)
        plt.plot(X, Y, "o-", label="first independent cluster")
        plt.xlabel("degree")
        plt.ylabel("clustering Coefficient")
        plt.title(" Population: " + str(pop))
        plt.legend(prop={"size": 10})
        fig.set_size_inches(w=width, h=height)
        if not show:
            plt.savefig(path + "an2_first_ind_c.pgf")
    if show:
        plt.show()
