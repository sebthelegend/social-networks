Those three python scripts enable you to construct and simulate social networks. Furthermore, they produce data for Gephi and graphxml.

social_networks.py:
Here you can construct and simulate specific networks with detailed modifyible structural attributes. You can also import a network from Facebook and compare two different networks. If you run this script you get a GUI in which you can modify your structural attributes.

social_networks_analyce:
Here you can run multiple simulations to get the average data of 48 networks. This script does not have a GUI, so you have to change the attributes in the script itself.

graph_building:
This is a helper script for both scripts above.
